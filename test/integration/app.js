import Competidor from '../../models/Competidor'

describe('Routes competidores', () => {
    const competidorDefault = {
        id: 1,
        nome: 'Joe Doe',
        idade: 22
    }

    // beforeEach(done => {
    //     Competidor
    //         .where(id == competidorDefault.id)
    //         .destroy()
    //         .then(destroyed => res.json({ destroyed }))
    //         .catch(err => {
    //             console.log(err);
    //             res.status(400).send({ err });
    //         });
    //     new Competidor(competidorDefault)
    //         .save()
    //         .then(saved => {
    //             res.json({ saved });
    //             done();
    //         })
    //         .catch(err => {
    //             console.log(err);
    //             res.status(400).send({ err });
    //         })
        
    // });

    describe('Route GET /competidor', () => {
        it('should return a list of competidores', done => {
            request
                .get('/competidor')
                .end((err, res) => {
                    expect(res.body[0].id).to.be.eql(competidorDefault.id)
                    expect(res.body[0].nome).to.be.eql(competidorDefault.nome)
                    expect(res.body[0].idade).to.be.eql(competidorDefault.idade)
                    done(err);
                });
        });
    });

})