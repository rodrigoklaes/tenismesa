import router from './routes/index';
import express from 'express';
import bodyParser from 'body-parser';

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const port = process.env.port || 3000
router(app);

app.listen(port, () => {
    console.log(`RUN!! Port: ${port}`);
});

