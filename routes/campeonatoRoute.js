import express from 'express';
import controller from '../controllers/campeonatoController';

const routes = express.Router();

routes
    //Busca todos os competidor
    .get('/', controller.getCampeonato)
    //Busca competidor por id
    .get('/:id', controller.getCampeonatoBy)
    //Insere competidor
    .post('/', controller.postCampeonato)    
    //Altera competidor
    .put('/:id', controller.putCampeonato)
    //Deleta competidor
    .delete('/:id', controller.deleteCampeonato)
    //Insere competidor no campeonato
    .post('/:id', controller.postCampeonatoCompetidor)

module.exports = app => app.use('/campeonato', routes);