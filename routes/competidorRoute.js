import express from 'express';
import controller from '../controllers/competidorController';

const routes = express.Router();

routes
    .get('/', controller.getCompetidor)
    .get('/:id', controller.getCompetidorBy)
    .get('/partidas/:competidor_id', controller.getCompetidorPartidas)
    .post('/', controller.postCompetidor)    
    .put('/:id', controller.putCompetidor)
    .delete('/:id', controller.deleteCompetidor)
    .post('/campeonato/:campeonato_id', controller.getCompetidorCampeonato)
module.exports = app => app.use('/competidor', routes);