import express from 'express';
import controller from '../controllers/partidasController';

const routes = express.Router();

routes
    .get('/', controller.getPartidas)
    .get('/:id', controller.getPartidasBy)
    .put('/status/:id', controller.putPartidasStatus)
    .post('/', controller.postPartidas)    
    .put('/:id', controller.putPartidas)
    .delete('/:id', controller.deletePartidas);

module.exports = app => app.use('/partidas', routes);