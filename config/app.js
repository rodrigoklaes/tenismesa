import router from '../routes/index';
import express from 'express';
import bodyParser from 'body-parser';

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const port = process.env.port || 3000
router(app);
app.router('/competidor').get((req, res ) => {
    res.json([{id: 1, nome: "Joe Doe", idade: 22}]);
});

export default app;