CREATE DATABASE IF NOT EXISTS tenisdemesa_nave;

CREATE TABLE IF NOT EXISTS competidor (
    id INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(70) NOT NULL,
    idade INT NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS campeonato (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(45) NOT NULL,
  qtdaCompetidor INT NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS partidas (
  id INT NOT NULL AUTO_INCREMENT,
  status VARCHAR(45) NOT NULL,
  campeonato_id INT NOT NULL,
  resultado VARCHAR(45) NULL,
  PRIMARY KEY (id, campeonato_id),
  INDEX fk_partidas_campeonato1_idx (campeonato_id ASC),
  CONSTRAINT fk_partidas_campeonato1
    FOREIGN KEY (campeonato_id)
    REFERENCES campeonato (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


CREATE TABLE IF NOT EXISTS competidor_campeonato (
  competidor_id INT NOT NULL,
  campeonato_id INT NOT NULL,
  PRIMARY KEY (competidor_id, campeonato_id),
  INDEX fk_competidor_campeonato_campeonato1_idx (campeonato_id ASC),
  INDEX fk_competidor_campeonato_competidor_idx (competidor_id ASC),
  CONSTRAINT fk_competidor_campeonato_competidor
    FOREIGN KEY (competidor_id)
    REFERENCES competidor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_competidor_campeonato_campeonato1
    FOREIGN KEY (campeonato_id)
    REFERENCES campeonato (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS competidor_partidas (
  competidor_id INT NOT NULL,
  partidas_id INT NOT NULL,
  PRIMARY KEY (competidor_id, partidas_id),
  INDEX fk_competidor_partidas_competidor1_idx (competidor_id ASC),
  CONSTRAINT fk_competidor_partidas_competidor1
    FOREIGN KEY (competidor_id)
    REFERENCES competidor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
