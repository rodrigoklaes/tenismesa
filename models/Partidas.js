import bookshelf from '../config/bookshelf';
import Campeonato from '../models/Campeonato';
import Competidor from '../models/Competidor';

const Partidas = bookshelf.Model.extend({
    tableName: 'partidas',
    competidor: function(){
        return this.belongsToMany(Competidor, 'competidor_partidas','partidas_id', 'competidor_id')
    },
    campeonato: function() {
        return this.belongsTo(Campeonato, 'campeonato_id', 'id')
    }

})

export default Partidas;