import bookshelf from '../config/bookshelf';
import Campeonato from '../models/Campeonato';
import Partidas from '../models/Partidas';

const Competidor = bookshelf.Model.extend({
    tableName: 'competidor',
    campeonato: function() {
        return this.belongsToMany(Campeonato, 'competidor_campeonato',  'competidor_id',  'campeonato_id');
    },
    partidas: function(){
        return this.belongsToMany(Partidas, 'competidor_partidas', 'competidor_id','partidas_id')
    }
})

export default Competidor;