import bookshelf from '../config/bookshelf';
import Competidor from '../models/Competidor';
import Partidas from '../models/Partidas';

const Campeonato = bookshelf.Model.extend({
    tableName: 'campeonato',
    competidor: function() {
        return this.belongsToMany(Competidor, 'competidor_campeonato',  'campeonato_id',  'competidor_id');
    },
    partidas: function () {
        return this.hasMany(Partidas, 'campeonato_id', 'id')
    }
});

export default Campeonato;