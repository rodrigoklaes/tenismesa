import Campeonato from "../models/Campeonato";
import Competidor from "../models/Competidor";
import Partidas from "../models/Partidas";

export default {
  getCampeonato: (req, res) => {
    try {
      Campeonato.fetchAll({
          withRelated: "competidor"
        })
        .then(campeonato => res.json({
          campeonato
        }))
        .catch(err => {
          console.log(err);
          res.status(400).send({
            err
          });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({
        error: "Erro ao listar campeonatoes"
      });
    }
  },
  getCampeonatoBy: (req, res) => {
    try {
      Campeonato.where(req.params)
        .fetchAll({
          withRelated: "competidor"
        })
        .then(campeonato => res.json({
          campeonato
        }))
        .catch(err => {
          console.log(err);
          res.status(400).send({
            err
          });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({
        error: "Erro ao listar campeonato"
      });
    }
  },
  postCampeonato: (req, res) => {
    try {
      const campeonato = new Campeonato(req.body)
      campeonato
        .save()
        .then(saved => {
          campeonato
            .competidor()
            .attach()
            .then(relation => res.json({
              saved,
              relation
            }))
        })
        .catch(err => {
          console.log(err);
          res.status(400).send({
            err
          });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({
        error: "Erro ao inserir campeonato"
      });
    }
  },
  putCampeonato: (req, res) => {
    try {
      Campeonato.where(req.params)
        .fetch()
        .then(Campeonato => {
          Campeonato.save(req.body).then(saved => res.json({
            saved
          }));
        })
        .catch(err => {
          console.log(err);
          res.status(400).send({
            err
          });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({
        error: "Erro ao alterar campeonato"
      });
    }
  },
  deleteCampeonato: (req, res) => {
    try {
      Campeonato.where(req.params)
        .destroy()
        .then(destroyed => res.json({
          destroyed
        }))
        .catch(err => {
          console.log(err);
          res.status(400).send({
            err
          });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({
        error: "Erro ao deletar campeonato"
      });
    }
  },
  postCampeonatoCompetidor: (req, res) => {
    try {
      const campeonato = Campeonato;
      const competidor = Competidor;
      campeonato
        .where(req.params)
        .fetch()
        .then(campeonato => {
          campeonato
            .save()
            .then(saved =>
              campeonato
              .competidor()
              .attach(req.body.competidor_id)
              .then(() => {

                let competidores = [];
                Competidor
                  .query(function (qb) {
                    qb.innerJoin('competidor_campeonato', 'competidor.id', 'competidor_campeonato.competidor_id');
                    qb.where('competidor_campeonato.campeonato_id', '=', req.params.id);
                  })
                  .fetchAll()
                  .then(data => {
                    if (data.length < 2) {
                      res.send("O número minímo de competidores para gerar partidas é de 2")
                    } else {
                      data.forEach(element => {
                        if (element.id != req.body.competidor_id) {
                          competidores.push(element.id);
                        }
                      });
                      for (let i = 0; i < competidores.length; i++) {
                        const idComp = req.body.competidor_id;
                        let idAdv = competidores[i];
                        const body = {
                          status: "Aguardando",
                          campeonato_id: req.params.id,
                          resultado: ""
                        }
                        let partidas = new Partidas(body);
                        partidas
                          .save()
                          .then(saved => {
                            const func = partidas.competidor();
                            Promise.all([
                              func.attach(idComp),
                              func.attach(idAdv)
                            ]).then(result => {
                              res.json({result});
                            });
                          })
                      }
                      
                    }
                  })
                  .catch(err => {
                    console.log(err);
                    res.status(400).send({
                      err
                    });
                  });

              })
            )
        })
        .catch(err => {
          console.log(err);
          res.status(400).send({
            err
          });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({
        error: "Erro ao inserir partidas"
      });
    }
  }
};