import Partidas from "../models/Partidas";

export default {
  getPartidas: (req, res) => {
    try {
      Partidas.fetchAll({ withRelated: ["competidor", "campeonato"] })
        .then(partidas => res.json({ partidas }))
        .catch(err => {
          console.log(err);
          res.status(400).send({ err });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({ error: "Erro ao listar partidases" });
    }
  },
  getPartidasBy: (req, res) => {
    try {
      Partidas.where(req.params)
        .fetchAll({ withRelated: ["competidor", "campeonato"] })
        .then(partidas => res.json({ partidas }))
        .catch(err => {
          console.log(err);
          res.status(400).send({ err });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({ error: "Erro ao listar partidas" });
    }
  },
  postPartidas: (req, res) => {
    try {
      const idComp = req.body.competidor_id;
      const idAdv = req.body.adversario_id;
      delete req.body.competidor_id;
      delete req.body.adversario_id;
      const partidas = new Partidas(req.body);
      partidas
        .save()
        .then(saved => {
          const func = partidas.competidor();
          Promise.all([
            func.attach(JSON.parse(idComp)),
            func.attach(JSON.parse(idAdv))
            ]
          ).then(result => {
            res.send(result);
          });
        })
        .catch(err => {
          console.log(err);
          res.status(400).send({ err });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({ error: "Erro ao inserir partidas" });
    }
  },
  putPartidas: (req, res) => {
    try {
      Partidas.where(req.params)
        .fetch()
        .then(Partidas => {
          Partidas.save(req.body).then(saved => res.json({ saved }));
        })
        .catch(err => {
          console.log(err);
          res.status(400).send({ err });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({ error: "Erro ao alterar partidas" });
    }
  },
  deletePartidas: (req, res) => {
    try {
      Partidas.where(req.params)
        .destroy()
        .then(destroyed => res.json({ destroyed }))
        .catch(err => {
          console.log(err);
          res.status(400).send({ err });
        });
    } catch (err) {
      console.log(err);
      return res.status(400).send({ error: "Erro ao deletar partidas" });
    }
  },
  putPartidasStatus: (req, res) => {
    try {
      if(req.body.resultComp == req.body.resultAdv){
        Partidas.where(req.params)
          .fetch()
          .then(Partidas => {
            Partidas.save({status: "Concluida", resultado: req.body.resultComp}).then(saved => res.json({ saved }));
          })
          .catch(err => {
            console.log(err);
            res.status(400).send({ err });
          });
      } else {
        res.send("Os resultados informado não são identicos");
      }
    } catch (err) {
      console.log(err);
      return res.status(400).send({ error: "Erro ao alterar partidas" });
    }
  }
};
